﻿using System;

namespace PriceList_Demo
{
    class PriceList
    {
        private double[] _prices = { 15.99, 27.88, 34.56, 45.89 };
        
        /// <summary>
        /// Get the price of an item
        /// </summary>
        /// <param name="index">item number</param>
        /// <returns>item price</returns>
        /// <exception cref="System.IndexOutOfRangeException">item number is invalid</exception>
        public double GetPrice(int index)
        {
            return _prices[index];
        }
    }
}
