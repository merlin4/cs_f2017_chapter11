﻿using System;

namespace PriceList_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            PriceList priceList = new PriceList();
            string price = "";
            try
            {
                Console.Write("Enter an item number: ");
                int item = Convert.ToInt32(Console.ReadLine());
                price = priceList.GetPrice(item).ToString("C");
            }
            catch (FormatException ex)
            {
                Console.WriteLine("invalid item number");
                price = "N/A";
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine("invalid item number");
                price = "N/A";
            }
            finally
            {
                Console.WriteLine();
                Console.WriteLine("PRICE: {0}", price);
            }
        }
    }
}
