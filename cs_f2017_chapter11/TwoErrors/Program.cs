﻿using System;

namespace TwoErrors
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 13;
            int denom = 0;
            int result;
            int[] arr = { 22, 33, 44 };

            try
            {
                result = num / denom;
                result = arr[num];
            }
            catch (DivideByZeroException ex)
            {
                Console.WriteLine("divide by zero: " + ex.Message);
            }
            catch (IndexOutOfRangeException ex)
            {
                Console.WriteLine("index out of range: " + ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("error: " + ex.Message);
            }
        }
    }
}
