﻿using System;

namespace PriceList2_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            PriceList priceList = new PriceList();
            string price = "";
            bool found = false;
            while (!found)
            {
                try
                {
                    Console.Write("Enter an item number: ");
                    int item = Convert.ToInt32(Console.ReadLine());
                    price = priceList.GetPrice(item).ToString("C");
                    found = true;
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("invalid item number");
                    price = "N/A";
                }
                catch (IndexOutOfRangeException ex)
                {
                    Console.WriteLine("invalid item number");
                    price = "N/A";
                }
            }

            Console.WriteLine();
            Console.WriteLine("PRICE: {0}", price);
        }
    }
}
