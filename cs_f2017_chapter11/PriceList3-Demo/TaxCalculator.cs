﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PriceList3_Demo
{
    class TaxCalculator
    {
        private double[] _taxRates = { 0.06, 0.07 };
        private double _priceCutoff = 20.00;

        public double DetermineTaxRate(double price)
        {
            int index = (price <= _priceCutoff ? 0 : 2);
            double rate = _taxRates[index];
            return rate;
        }
    }
}
