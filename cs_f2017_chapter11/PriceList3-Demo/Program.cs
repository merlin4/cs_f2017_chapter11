﻿using System;

namespace PriceList3_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            PriceList priceList = new PriceList();
            string price = "";
            bool found = false;
            while (!found)
            {
                try
                {
                    Console.Write("Enter an item number: ");
                    int item = Convert.ToInt32(Console.ReadLine());
                    price = priceList.GetPrice(item).ToString("C");
                    found = true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                    Console.WriteLine("error");
                    Console.WriteLine(ex.ToString());
                    Console.WriteLine();
                    price = "N/A";
                }
            }

            Console.WriteLine();
            Console.WriteLine("PRICE: {0}", price);
        }
    }
}
