﻿using System;

namespace PriceList3_Demo
{
    class PriceList
    {
        private double[] _prices = { 15.99, 27.88, 34.56, 45.89 };
        private TaxCalculator _tax = new TaxCalculator();

        /// <summary>
        /// Get the price of an item
        /// </summary>
        /// <param name="index">item number</param>
        /// <returns>item price</returns>
        /// <exception cref="System.IndexOutOfRangeException">item number is invalid</exception>
        public double GetPrice(int index)
        {
            double price = _prices[index];
            double taxRate = _tax.DetermineTaxRate(price);
            double total = price * (1 + taxRate);
            return total;
        }
    }
}
