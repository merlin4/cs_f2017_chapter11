﻿using System;

namespace MPG_Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.Write("Enter miles driven: ");
                int miles = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter gallons of gas purchased: ");
                int gallons = Convert.ToInt32(Console.ReadLine());

                int mpg = miles / gallons;
                Console.WriteLine("You got {0} miles per gallon", mpg);
            }
            catch (Exception ex)
            {
                Console.WriteLine();
                Console.WriteLine("Something went wrong");
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
