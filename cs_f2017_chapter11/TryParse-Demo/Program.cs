﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryParse_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            int value1 = Prompt1("enter first value", 5);
            int value2 = Prompt2("enter second value", 7);

            Console.WriteLine();
            Console.WriteLine("PARSED VALUES: {0}, {1}", value1, value2);
        }

        static int Prompt1(string prompt, int defaultValue)
        {
            Console.WriteLine(prompt);
            string input = Console.ReadLine();
            int value;
            if (int.TryParse(input, out value))
            {
                return value;
            }
            else
            {
                return defaultValue;
            }
        }

        static int Prompt2(string prompt, int defaultValue)
        {
            Console.WriteLine(prompt);
            string input = Console.ReadLine();
            try
            {
                int value = int.Parse(input);
                return value;
            }
            catch (ArgumentException ex)
            {
                return defaultValue;
            }
            catch (FormatException ex)
            {
                return defaultValue;
            }
            catch (OverflowException ex)
            {
                return defaultValue;
            }
        }
    }
}
