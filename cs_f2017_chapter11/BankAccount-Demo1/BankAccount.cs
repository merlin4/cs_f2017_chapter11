﻿using System;

namespace BankAccount_Demo1
{
    class BankAccount
    {
        private int _accountNumber;
        private double _balance;
        
        public int AccountNumber
        {
            get { return _accountNumber; }
            set { _accountNumber = value; }
        }

        public double Balance
        {
            get { return _balance; }
            set
            {
                if (value < 0)
                {
                    throw new NegativeBalanceException();
                }
                else
                {
                    _balance = value;
                }
            }
        }
    }
}
