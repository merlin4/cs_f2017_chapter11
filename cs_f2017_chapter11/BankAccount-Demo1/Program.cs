﻿using System;

namespace BankAccount_Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount account = new BankAccount();
            try
            {
                account.AccountNumber = 1234;
                account.Balance = -1000;
            }
            catch (NegativeBalanceException ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                //Console.WriteLine(ex.ToString());
            }
        }
    }
}
