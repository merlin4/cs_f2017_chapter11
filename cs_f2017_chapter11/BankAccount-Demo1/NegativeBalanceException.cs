﻿using System;

namespace BankAccount_Demo1
{
    class NegativeBalanceException : ApplicationException
    {
        public const string DEFAULT_MESSAGE = "Bank balance would be negative.";

        public NegativeBalanceException() : base(DEFAULT_MESSAGE)
        {
        }

        public NegativeBalanceException(string message) : base(message)
        {
        }
    }
}
