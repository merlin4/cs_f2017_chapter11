﻿using System;

namespace Rethrow_Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Trying in Main() method");
                MethodA();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Caught in Main() method: " + ex.Message);
            }
            finally
            {
                Console.WriteLine("Main() method is done");
            }
        }

        static void MethodA()
        {
            try
            {
                Console.WriteLine("Trying in method A");
                MethodB();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Caught in method A: " + ex.Message);
                throw ex;
            }
        }

        static void MethodB()
        {
            try
            {
                Console.WriteLine("Trying in method B");
                MethodC();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Caught in method B: " + ex.Message);
                throw ex;
            }
        }

        static void MethodC()
        {
            Console.WriteLine("In method C");
            throw new Exception("This came from method C");
        }
    }
}
